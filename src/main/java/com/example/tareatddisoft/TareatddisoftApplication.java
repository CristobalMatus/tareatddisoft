package com.example.tareatddisoft;

import com.example.tareatddisoft.models.Usuario;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TareatddisoftApplication {

    public static void main(String[] args) {SpringApplication.run(TareatddisoftApplication.class, args);}

}
