package com.example.tareatddisoft.controllers;
import com.example.tareatddisoft.models.Usuario;
import com.example.tareatddisoft.services.ServicioUsuario;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/usuarios")
public class ControladorUsuario {

    private final ServicioUsuario servicioUsuario;

    public ControladorUsuario(ServicioUsuario servicioUsuario) {
        this.servicioUsuario = servicioUsuario;
    }

    @PostMapping("/crear")
    public String crearUsuario(@RequestBody Usuario usuario) {
        boolean esValido = servicioUsuario.validarUsuario(usuario);

        if (esValido) {
            return "Usuario creado exitosamente";
        } else {
            return "Usuario inválido";
        }
    }
}
