package com.example.tareatddisoft.services;

import com.example.tareatddisoft.models.Usuario;
import org.springframework.stereotype.Service;

@Service
public class ServicioUsuario {

    public boolean validarUsuario(Usuario usuario) {
        return esRutValido(usuario.getRut()) &&
                esNumeroTelefonicoValido(usuario.getNumeroTelefonico()) &&
                usuario.getEdad() > 0 && usuario.getEdad() < 150 &&
                contieneSoloLetras(usuario.getNombre()) &&
                contieneSoloLetras(usuario.getApellidoPaterno()) &&
                contieneSoloLetras(usuario.getApellidoMaterno());
    }

    private boolean contieneSoloLetras(String texto) {
        return texto.matches("[\\p{L}]+");
    }

    private boolean esRutValido(String rut)  {

        boolean validacion = false;
        try {
            rut =  rut.toUpperCase();
            rut = rut.replace(".", "");
            rut = rut.replace("-", "");
            int rutAux = Integer.parseInt(rut.substring(0, rut.length() - 1));

            char dv = rut.charAt(rut.length() - 1);

            int m = 0, s = 1;
            for (; rutAux != 0; rutAux /= 10) {
                s = (s + rutAux % 10 * (9 - m++ % 6)) % 11;
            }
            if (dv == (char) (s != 0 ? s + 47 : 75)) {
                validacion = true;
            }

        } catch (Exception e) {
            System.out.println("Error al validar");
        }
        return validacion;
    }

    private boolean esNumeroTelefonicoValido(String numeroTelefonico) {
        return numeroTelefonico.matches("\\+569\\d{8}");
    }

}
