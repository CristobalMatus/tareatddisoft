package com.example.tareatddisoft.services;

import com.example.tareatddisoft.models.Usuario;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class ServicioUsuarioTest {

    private ServicioUsuario servicioUsuario;
    private Usuario usuario;

    @BeforeEach
    public void setUp() {
        servicioUsuario = new ServicioUsuario();
        usuario = new Usuario();
        usuario.setNombre("Cristóbal");
        usuario.setApellidoPaterno("Matus");
        usuario.setApellidoMaterno("Padilla");
        usuario.setRut("21008298-0");
        usuario.setNumeroTelefonico("+56912345678");
        usuario.setEdad(21);
    }

    @ParameterizedTest
    @ValueSource(strings = {"-10", "0", "150","200"})
    @DisplayName("Prueba validarUsuario: Edad inválida")
    public void edadInvalida(String edad) {
        usuario.setEdad(Integer.parseInt(edad)); // Establecer la edad para cada prueba

        boolean resultado = servicioUsuario.validarUsuario(usuario);

        Assertions.assertFalse(resultado);
    }

    @ParameterizedTest
    @ValueSource(strings = {"5", "17", "54","30","149"})
    @DisplayName("Prueba validarUsuario: Edad válida")
    public void edadValida(String edad) {
        usuario.setEdad(Integer.parseInt(edad)); // Establecer la edad para cada prueba

        boolean resultado = servicioUsuario.validarUsuario(usuario);

        Assertions.assertTrue(resultado);
    }

    @ParameterizedTest
    @ValueSource(strings = {"21008298-7", "12345678-9"})
    @DisplayName("Prueba validarUsuario: Rut inválido")
    public void rutInvalido(String rut) {
        usuario.setRut(rut); // Establecer el rut para cada prueba

        boolean resultado = servicioUsuario.validarUsuario(usuario);

        Assertions.assertFalse(resultado);
    }

    @ParameterizedTest
    @ValueSource(strings = {"+5612345678239", "+5691234567", "+5691234567890"})
    @DisplayName("Prueba validarUsuario: Número telefónico inválido")
    public void numeroTelefonicoInvalido(String numeroTelefonico) {
        usuario.setNumeroTelefonico(numeroTelefonico); // Establecer el número telefónico para cada prueba

        boolean resultado = servicioUsuario.validarUsuario(usuario);

        Assertions.assertFalse(resultado);
    }

    @ParameterizedTest
    @ValueSource(strings = {"21008298-0", "10311807-7"})
    @DisplayName("Prueba validarUsuario: Rut válido")
    public void rutValido(String rut) {
        usuario.setRut(rut); // Establecer el rut para cada prueba

        boolean resultado = servicioUsuario.validarUsuario(usuario);

        Assertions.assertTrue(resultado);
    }

    @ParameterizedTest
    @ValueSource(strings = {"+56912345678", "+56987654321"})
    @DisplayName("Prueba validarUsuario: Número telefónico válido")
    public void numeroTelefonicoValido(String numeroTelefonico) {
        usuario.setNumeroTelefonico(numeroTelefonico); // Establecer el número telefónico para cada prueba

        boolean resultado = servicioUsuario.validarUsuario(usuario);

        Assertions.assertTrue(resultado);
    }

    @ParameterizedTest
    @ValueSource(strings = {"123", "John123", "Mary-Sue"})
    @DisplayName("Prueba validarUsuario: Nombre inválido")
    public void nombreInvalido(String nombre) {
        usuario.setNombre(nombre); // Establecer el nombre para cada prueba

        boolean resultado = servicioUsuario.validarUsuario(usuario);

        Assertions.assertFalse(resultado);
    }

    @ParameterizedTest
    @ValueSource(strings = {"John", "Mary", "Jane"})
    @DisplayName("Prueba validarUsuario: Nombre válido")
    public void nombreValido(String nombre) {
        usuario.setNombre(nombre); // Establecer el nombre para cada prueba

        boolean resultado = servicioUsuario.validarUsuario(usuario);

        Assertions.assertTrue(resultado);
    }

    @ParameterizedTest
    @ValueSource(strings = {"123", "Doe123", "Smith-Johnson"})
    @DisplayName("Prueba validarUsuario: Apellido paterno inválido")
    public void apellidoPaternoInvalido(String apellidoPaterno) {
        usuario.setApellidoPaterno(apellidoPaterno); // Establecer el apellido paterno para cada prueba

        boolean resultado = servicioUsuario.validarUsuario(usuario);

        Assertions.assertFalse(resultado);
    }

    @ParameterizedTest
    @ValueSource(strings = {"Doe", "Smith", "Johnson"})
    @DisplayName("Prueba validarUsuario: Apellido paterno válido")
    public void apellidoPaternoValido(String apellidoPaterno) {
        usuario.setApellidoPaterno(apellidoPaterno); // Establecer el apellido paterno para cada prueba

        boolean resultado = servicioUsuario.validarUsuario(usuario);

        Assertions.assertTrue(resultado);
    }

    @ParameterizedTest
    @ValueSource(strings = {"123", "Doe123", "Smith-Johnson"})
    @DisplayName("Prueba validarUsuario: Apellido materno inválido")
    public void apellidoMaternoInvalido(String apellidoMaterno) {
        usuario.setApellidoMaterno(apellidoMaterno); // Establecer el apellido materno para cada prueba

        boolean resultado = servicioUsuario.validarUsuario(usuario);

        Assertions.assertFalse(resultado);
    }

    @ParameterizedTest
    @ValueSource(strings = {"Doe", "Smith", "Johnson"})
    @DisplayName("Prueba validarUsuario: Apellido materno válido")
    public void apellidoMaternoValido(String apellidoMaterno) {
        usuario.setApellidoMaterno(apellidoMaterno); // Establecer el apellido materno para cada prueba

        boolean resultado = servicioUsuario.validarUsuario(usuario);

        Assertions.assertTrue(resultado);
    }

}
